import DS from 'ember-data';
import Instance from './instance';

export default Instance.extend({
  base: DS.belongsTo('persona-assignment'),
  enabled: DS.attr('boolean'),
  overrideConfig: DS.attr('string'),
  result: DS.attr('string')
});
