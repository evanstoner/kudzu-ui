import DS from 'ember-data';

export default DS.Model.extend({
  ipAddress: DS.attr('string'),
  host: DS.belongsTo('host', { async: true }),
  network: DS.belongsTo('network', { async: true }),
  testbed: DS.belongsTo('testbed', { async: true })
});
