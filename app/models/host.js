import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  name: DS.attr('string'),
  accessiblePorts: DS.attr('array'),
  image: DS.belongsTo('image', { async: true }),
  flavor: DS.belongsTo('flavor', { async: true }),
  testbed: DS.belongsTo('testbed', { async: true }),
  links: DS.hasMany('link', { aysnc: true }),
  personaAssignments: DS.hasMany('personaAssignment', { async: true }),

  personaAssignmentsSorted: Ember.computed.sort('personaAssignments', function(a, b) {
    return a.get('priority') - b.get('priority');
  })
});
