import DS from 'ember-data';
import Instance from './instance';

export default Instance.extend({
  base: DS.belongsTo('host'),
  accessIpAddress: DS.attr('string'),
  accessPort: DS.attr('number'),
  accessError: DS.attr('string')
});
