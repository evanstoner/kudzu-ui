import DS from 'ember-data';
import Instance from './instance';

export default Instance.extend({
  base: DS.belongsTo('link')
});
