import DS from 'ember-data';
import Instance from './instance';

export default Instance.extend({
  //FIXME: uses the naming workaround, see routerx.js
  base: DS.belongsTo('routerx'),
  accessDetails: DS.attr('string'),
  prepared: DS.attr('boolean')
});
