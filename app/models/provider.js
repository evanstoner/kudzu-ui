import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  testbedDriver: DS.attr('string'),
  testbedConfig: DS.attr('string'),
  routerDriver: DS.attr('string'),
  routerConfig: DS.attr('string'),

  testbedDriverSimple: function() {
    var driver = this.get('testbedDriver');
    if (driver) {
      return driver.substr(driver.lastIndexOf('.') + 1);
    } else {
      return null;
    }
  }.property('testbedDriver'),

  routerDriverSimple: function() {
    var driver = this.get('routerDriver');
    if (driver) {
      return driver.substr(driver.lastIndexOf('.') + 1);
    } else {
      return null;
    }
  }.property('routerDriver')
});
