import DS from 'ember-data';

export default DS.Model.extend({
  priority: DS.attr('number'),
  config: DS.attr('string'),
  persona: DS.belongsTo('persona', { async: true }),
  host: DS.belongsTo('host', { async: true })
});
