import DS from 'ember-data';

/**
* FIXME: if we name this model 'router', it collides with the application router;
* then we see errors like 'kudzu-ui@router:model does not appear to be an
* ember-data model'
*/

export default DS.Model.extend({
  type: DS.attr('string'),
  testbed: DS.belongsTo('testbed'),

  typeHuman: function() {
    var type = this.get('type');
    if (type) {
      return type.toLowerCase();
    } else {
      return '';
    }
  }.property('type')
});
