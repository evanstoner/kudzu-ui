import Ember from 'ember';

export default Ember.Route.extend({

  model() {
    var buildQuery = {
      build: this.modelFor('builds.build').get('id')
    };
    var testbedQuery = {
      testbed: this.modelFor('builds.build').get('testbed.id')
    };

    // the store query returns an array, but each will only contain one object
    // since a Build only contains one RouterInstance, and a Testbed only contains
    // one Router. so, after the query promise is resolved, return a new promise
    // that will immediately resolve containing the firstObject of each array
    return Ember.RSVP.hash({
      routerInstances: this.store.query('router-instance', buildQuery),
      routers: this.store.query('routerx', testbedQuery),
      hostInstances: this.store.query('host-instance', buildQuery),
      hosts: this.store.query('host', testbedQuery)
    }).then(function(model) {
      return Ember.RSVP.hash({
        routerInstance: model.routerInstances.get('firstObject'),
        router: model.routers.get('firstObject'),
        hostInstances: model.hostInstances,
        hosts: model.hosts
      });
    });
  }

});
