import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    var buildQuery = {
      build: this.modelFor('builds.build').get('id')
    };
    var testbedQuery = {
      testbed: this.modelFor('builds.build').get('testbed.id')
    };
    // load the instances, and also their bases to save trips to the server
    return Ember.RSVP.hash({
      hostInstances:    this.store.query('host-instance', buildQuery),
      networkInstances: this.store.query('network-instance', buildQuery),
      linkInstances:    this.store.query('link-instance', buildQuery),
      personaAssignmentInstances: this.store.query('persona-assignment-instance', buildQuery),
      hosts:    this.store.query('host', testbedQuery),
      networks: this.store.query('network', testbedQuery),
      links:    this.store.query('link', testbedQuery),
      personaAssignments: this.store.query('persona-assignment', testbedQuery)
    });
  }
});
