import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('project');
  },

  actions: {
    /**
     * Destroy the temporary model if it was not persisted using a save action.
     */
    willTransition() {
      var model = this.controller.get('model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    },

    /**
     * Save (create) the model.
     */
    save() {
      var self = this;
      var model = this.controller.get('model');
      model.save().then(function() {
        self.transitionTo('projects');
      });
    }
  }
});
