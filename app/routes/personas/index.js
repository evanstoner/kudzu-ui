import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      personas: this.store.findAll('persona'),
      provisioners: this.store.findAll('provisioner')
    });
  }
});
