import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    // find networks who belong to the testbed
    return this.store.query('network', {
      testbed: this.modelFor('testbed').get('id')
    });
  },

  actions: {
    createNetwork() {
      var newNetwork = this.controllerFor('testbed.networks').get('newNetwork');

      if (!newNetwork.name) {
        Ember.set(newNetwork, 'name', 'new network');
      }
      if (!newNetwork.cidr) {
        Ember.set(newNetwork, 'cidr', '192.168.1.0/24');
      }
      newNetwork.testbed = this.modelFor('testbed');

      var network = this.store.createRecord('network', newNetwork);
      network.save();

      this.controllerFor('testbed.networks').set('newNetwork', {});
      this.get('currentModel').addObject(network._internalModel);
    },

    deleteNetwork(network) {
      if (confirm(
`Permanently delete network?

Network: ${network.get('name')}
CIDR: ${network.get('cidr')}`
      )) {
        network.destroyRecord();
      }
    }
  }
});
