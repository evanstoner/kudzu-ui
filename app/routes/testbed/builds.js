import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.query('build', {
      testbed: this.modelFor('testbed').get('id')
    });
  },

  actions: {
    createBuild() {
      var newBuild = this.controllerFor('testbed.builds').get('newBuild');

      if (!newBuild.name) {
        newBuild.name = 'new build';
      }
      newBuild.testbed = this.modelFor('testbed');

      var build = this.store.createRecord('build', newBuild);
      build.save();

      this.controllerFor('testbed.builds').set('newBuild', {});
      this.get('currentModel').addObject(build._internalModel);
    }
  }
});
