import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    var testbedQuery = {
      testbed: this.modelFor('testbed').get('id')
    };
    var providerQuery = {
      provider: this.modelFor('testbed').get('provider.id')
    };
    // we know what resources we're going to need to display all the records
    // appropriately -- just fetch them all now rather than letting relationships
    // force many small requests
    return Ember.RSVP.hash({
      hosts: this.store.query('host', testbedQuery),
      links: this.store.query('link', testbedQuery),
      personaAssignments: this.store.query('personaAssignment', testbedQuery),
      images: this.store.query('image', providerQuery),
      flavors: this.store.query('flavor', providerQuery),
      //TODO: query only personas used by this testbed's assignments
      personas: this.store.findAll('persona')
    });
  },

  actions: {
    createHost() {
      var newHost = this.controllerFor('testbed.hosts').get('newHost');
      var newHostNetworks = this.controllerFor('testbed.hosts').get('newHostNetworks');
      var testbed = this.modelFor('testbed');
      var self = this;

      // working around x-select's failure to set the selection to the first item
      // https://github.com/thefrontside/emberx-select/issues/81
      if (!newHost.image) {
        Ember.set(newHost, 'image', this.get('currentModel.images.firstObject'));
      }
      if (!newHost.flavor) {
        Ember.set(newHost, 'flavor', this.get('currentModel.flavors.firstObject'));
      }

      if (!newHost.name) {
        Ember.set(newHost, 'name', 'new host');
      }
      Ember.set(newHost, 'testbed', testbed);

      var host = this.store.createRecord('host', newHost);
      var links = newHostNetworks.map(function(network) {
        return self.store.createRecord('link', {
          host: host,
          network: network,
          testbed: testbed,
          // it's ok if ipAddress is undefined; Kudzu will choose one for us
          ipAddress: network.get('newHostAddress')
        });
      });

      host.save().then(function() {
        links.forEach(function(link) {
          link.save();
          self.get('currentModel.links').addObject(link._internalModel);
        });
      });

      // keep the same image and flavor selected but reset the name; and
      // intentionally do not reset newHostNetworks
      this.controllerFor('testbed.hosts').set('newHost', {
        image: newHost.image,
        flavor: newHost.flavor
      });
      this.get('currentModel.hosts').addObject(host._internalModel);
    },

    deleteHost(host) {
      if (confirm(
`Permanently delete host?

Host: ${host.get('name')}`
      )) {
        host.destroyRecord();
      }
    },

    deleteLink(link) {
      if (confirm(
`Permanently delete link?

Host: ${link.get('host.name')}
Network: ${link.get('network.name')}
Address: ${link.get('ipAddress')}`
      )) {
        link.destroyRecord();
      }
    },

    createPersonaAssignment() {
      var newPersonaAssignment = this.controllerFor('testbed.hosts').get('newPersonaAssignment');

      if (!newPersonaAssignment.priority) {
        Ember.set(newPersonaAssignment, 'priority', 50);
      }

      var personaAssignment = this.store.createRecord('personaAssignment', newPersonaAssignment);
      personaAssignment.save();

      this.get('currentModel.personaAssignments').addObject(personaAssignment._internalModel);

      this.controllerFor('testbed.hosts').set('assignPersonaDialogOpen', false);
      this.controllerFor('testbed.hosts').set('newPersonaAssignment', {
        persona: newPersonaAssignment.persona
      });
    },

    deletePersonaAssignment(pa) {
      if (confirm(
`Permanently delete persona assignment?

Persona: ${pa.get('persona.name')}
Host: ${pa.get('host.name')}`
      )) {
        pa.destroyRecord();
      }
    }
  }
});
