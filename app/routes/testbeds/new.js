import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      projects: this.store.findAll('project'),
      providers: this.store.findAll('provider')
    });
  },

  actions: {
    /**
     * Save (create) the model.
     */
    save() {
      var self = this;
      var newTestbed = this.controller.get('newTestbed');

      if (!newTestbed.name) {
        alert('Testbed name is required.');
        return;
      }

      if (!newTestbed.project) {
        Ember.set(newTestbed, 'project', this.get('currentModel.projects.firstObject'));
      }

      if (!newTestbed.provider) {
        Ember.set(newTestbed, 'provider', this.get('currentModel.providers.firstObject'));
      }

      var testbed = this.store.createRecord('testbed', newTestbed);

      testbed.save().then(function(model) {
        self.transitionTo('testbed.hosts', model);
      });

      this.controller.set('newTestbed', {});
    }
  }
});
