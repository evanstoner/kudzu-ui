import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    willTransition() {
      this.controller.set('newProvider', {});
    },
    
    save() {
      var self = this;
      var newProvider = this.controller.get('newProvider');
      var provider = this.store.createRecord('provider', newProvider);
      provider.save().then(function() {
        self.transitionTo('admin.providers');
      });
    },

    cancel() {
      this.transitionTo('admin.providers');
    }
  }
});
