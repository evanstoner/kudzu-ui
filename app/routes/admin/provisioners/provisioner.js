import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('provisioner', params.provisioner_id);
  },

  actions: {
    save() {
      var model = this.controller.get('model');
      model.save();
      alert('Saved.');
      this.transitionTo('admin.provisioners');
    },

    cancel() {
      var model = this.controller.get('model');
      model.rollbackAttributes();
      this.transitionTo('admin.provisioners');
    }
  }
});
