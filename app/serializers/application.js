import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  serialize: function(record, options) {
    options = options ? options : {};
    // always include the resource's ID in the request body, not just the URL 
    options.includeId = true;
    return this._super.apply(this, [record, options]);
  }
});
