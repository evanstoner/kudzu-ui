import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('testbeds', function() {
    this.route('new');
  });
  this.route('testbed', { path: '/testbeds/:testbed_id' }, function() {
    this.route('hosts');
    this.route('networks');
    this.route('settings');
    this.route('builds');
  });
  this.route('projects', function() {
    this.route('new');
  });
  this.route('loading');

  this.route('builds', function() {
    this.route('build', { path: '/:build_id' }, function() {
      this.route('instances');
      this.route('access');
      this.route('lifecycle');
    });
  });
  this.route('admin', function() {
    this.route('providers', function() {
      this.route('provider', { path: '/:provider_id' });
      this.route('new');
    });
    this.route('provisioners', function() {
      this.route('provisioner', { path: '/:provisioner_id' });
      this.route('new');
    });
  });
  this.route('personas', function() {});
});

export default Router;
