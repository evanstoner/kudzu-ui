import Ember from 'ember';
import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    return (Ember.typeOf(serialized) === "array") ? serialized : [];
  },

  serialize(deserialized) {
    return (Ember.typeOf(deserialized) === "array") ? deserialized : [];
  }
});
