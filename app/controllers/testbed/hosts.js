import Ember from 'ember';

export default Ember.Controller.extend({
  testbedController: Ember.inject.controller('testbed'),
  testbed: Ember.computed.alias('testbedController.model'),

  newHostFormOpen: true,
  newHost: {},
  newHostNetworks: Ember.computed.filterBy('testbed.nonServiceNetworks', 'selected'),

  assignPersonaDialogOpen: false,
  newPersonaAssignment: {},

  actions: {
    toggleNewHostForm() {
      this.toggleProperty('newHostFormOpen');
    },

    openAssignPersonaDialog(host) {
      this.set('newPersonaAssignment.host', host);
      this.set('assignPersonaDialogOpen', true);
    }
  }

});
