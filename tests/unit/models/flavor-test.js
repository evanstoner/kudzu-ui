import { moduleForModel, test } from 'ember-qunit';

moduleForModel('flavor', 'Unit | Model | flavor', {
  // Specify the other units that are required for this test.
  needs: ['model:provider']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
