import { moduleForModel, test } from 'ember-qunit';

moduleForModel('build/host-instance', 'Unit | Model | build/host instance', {
  // Specify the other units that are required for this test.
  needs: ['model:base']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
